extern crate serenity;
extern crate dotenv;

use serenity::client::{Client, Context};
use serenity::prelude::*;
use serenity::model::channel::Message;
use serenity::framework::standard::{Args, CommandOptions, CommandError, StandardFramework};
use std::env;
use dotenv::dotenv;

struct Handler;
impl EventHandler for Handler { }

fn main () {
    dotenv().ok();
    dotenv::dotenv().expect("Failed to read .env file");

    let mut client = Client::new(&env::var("DISCORD_TOKEN").expect("token"), Handler).expect("Error creating client");

    client.with_framework(StandardFramework::new()
        .configure(|c| c.prefix("i."))
        .command("ping", |c| c
            .desc("Replies to ping with a pong.")
            .exec(ping))
        .command("status", |c| c
            .check(owner_check)
            .desc("Changes bot status.")
            .exec(status))
        .command("info", |c| c
            .desc("Displays bot information.")
            .exec(info)));

    if let Err(why) = client.start() {
        println!("An error occured while running the client: {:?}", why);
    }
}

fn ping(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    message.channel_id.say("Pong!")?;

    Ok(())
}

fn status(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    use serenity::model::gateway::Game;

    let argu = message.content.splitn(2, ' ').collect::<Vec<&str>>();
    let start = *unsafe { argu.get_unchecked(0) };
    let game = *unsafe { argu.get_unchecked(1) };

    if argu.len() < 2 {
        println!("Failed to set status: parse failed.")
    } else if start !=  "i.status" {
        println!("Failed to set status: parse failed.")
    } else {
        _context.set_game(Game::listening(game));
        println!("Status successfully changed to: {:?}", game)
    };

    Ok(())
}

fn info(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    let _ = message.channel_id.send_message( |m| m 
        .embed(|e| e
            .color(0x23AD00)
            .title("__General Information__")
            .field("Library", "[Serenity](https://github.com/zeyla/serenity)", true)
            .field("Help", "If you need help, find a bug, or have a general question then feel free to contact coopyey#1337.", false)));
   
   Ok(())
}

fn owner_check(_context: &mut Context, message: &Message, _: &mut Args, _: &CommandOptions) -> bool {
    let temp = &env::var("OWNERID").expect("ownerid");
    let owner: u64 = temp.parse().unwrap();
    message.author.id == owner
}