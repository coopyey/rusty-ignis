# rusty-ignis
A discord bot written in Rust.

### Features that will be added:
Simple moderation features such as mute, kick, ban, etc
Role management
Channel management
FFXIV specific commands for lodestone and fflogs
Maybe some WoW commands maybe but who knows

### Installation
This bot uses the Serenity discord library as well as the Dotenv crate.

### Who do I talk to?
coopyey#1337 maintains this bot. If you need any help please pm Coop.